const http = require('http')
const port = 3000

const server = http.createServer((request, response) => {
	if(request.url == '/register') {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Im sorry the page you are looking for cannot be found.')
	} else {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('You are in Login Page!')
	}
})
server.listen(port)

console.log(`Server is succesfully running:${port}`)