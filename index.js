 // 1. What directive is used by Node.js in loading the modules it needs?
// > Require Directive


// 2. What Node.js module contains a method for server creation?
// > http module


// 3. What is the method of the http object responsible for creating a server using Node.js?
// > Create Server

// 4. What method of the response object allows us to set status codes and content types?
// > setStatus

// 5. Where will console.log() output its contents when run in Node.js?
// > browser


// 6. What property of the request object contains the address's endpoint?
// > Request Object Property


let http = require('http')
let port = 3000

http.createServer((request, response) => {
	if(request.url == '/register') {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end('Im sorry the page you are looking for cannot be found.')
	} else {
	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('You are in Login Page!')
	}
}).listen(port)

console.log(`Server is succesfully running:${port}`)
